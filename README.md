This project provides two tools for networking testing.

To compile....

Install pcap libraries for developing.

After that execute:

$ make clean
$ make

use: You can use galena (one of the application to sniff the packets.

This is an example for sniff from eth0

$ sudo ./galena 0.0.0.0 -d eth0 -sniffer -analizar

or use wireshark

To genereate packages.

get one of the cfg files at examples directory and customize it.
In the future i will provide a user manual, is quite intuitive reading cgf files

note the lines RUN and DELAY

DELAY <milliseconds>
time between bursts of packets

RUN <times>

Number of burst to send (a value of zero means infinite)


Notation tips

a + sign means increment a field in the next burst
a X..X means a range of values
an ip X.X.X.X/X+ means increment the ip address at next step belonging to theis subnet
an ip X.X.X.X/* means successive ramdom adresses in this subnet

0xXX is an8 bit hex value
0xXXX is an 16 bit hex value
"A string" is an 8 bit encoded ASCII string
XXX is an decimal value
FCS=0xXXXX at IP, TCP, UDP, ICMP headers forces checksum heather to that value


Exlore the samples to have an idea of how it works


Use:

$ sudo ./ethergen eth0 samples/mi_test.cgf 
Sends traffic to eth0 described at mi_pest.cfg


$ ./ethergen Poutput.pcap samples/mi_test.cgf 
generates a pacap file called output.pcap, (useful to check or to send it with other tools


Any questions must be sent to

Amanda Azanon

teruel@tid.es

The subject must start with @Sauco to help me to clasify my email

Enjoy :)




